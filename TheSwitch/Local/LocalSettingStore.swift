//
//  LocalSettingStore.swift
//  TheSwitch
//
//  Created by Daniel Lehtovirta on 23/04/2019.
//  Copyright © 2019 Daniel Lehtovirta. All rights reserved.
//

import UIKit

//Used for storing current switch state locally in NSUserDefaults
enum SwitchConfiguration {
    static let userDefaultsSwitchState = "switchState"
}

class LocalSettingStore: NSObject {
    
    static func switchState() -> Bool {
        
        let switchState = UserDefaults.standard.bool(forKey: SwitchConfiguration.userDefaultsSwitchState)
        return switchState
    }
    
    static func switchChanged(switchState: Bool) {
        UserDefaults.standard.set(switchState, forKey: SwitchConfiguration.userDefaultsSwitchState)
    }
}
