//
//  SwitchState.swift
//  TheSwitch
//
//  Created by Daniel Lehtovirta on 23/04/2019.
//  Copyright © 2019 Daniel Lehtovirta. All rights reserved.
//

struct SwitchState: Codable {
    
    let state: Bool?
    
    enum CodingKeys: String, CodingKey {
        case state = "state"
    }
}
