//
//  Constants.swift
//  TheSwitch
//
//  Created by Daniel Lehtovirta on 24/04/2019.
//  Copyright © 2019 Daniel Lehtovirta. All rights reserved.
//

import UIKit

enum Network {
    static let baseUrl: URL = URL(string: "http://localhost:3000")!
    static let networkRetries: Int = 4
    
    static let scheduledTimer: TimeInterval = 60.0
}

enum HeaderSetting {
    static let contentTypeHeader = "Content-Type"
    static let contentTypeJSON = "application/json; charset=utf-8"
}

enum HTTPMethod {
    static let put = "PUT"
    static let get = "GET"
}
