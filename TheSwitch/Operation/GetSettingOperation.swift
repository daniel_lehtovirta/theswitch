//
//  GetSettingOperation.swift
//  TheSwitch
//
//  Created by Daniel Lehtovirta on 23/04/2019.
//  Copyright © 2019 Daniel Lehtovirta. All rights reserved.
//

import UIKit

typealias GetSettingsCompletionBlock = (_ operation: GetSettingOperation?) -> Void

class GetSettingOperation: AsynchronousOperation {
    
    var completedState: SwitchState?
    var completion: GetSettingsCompletionBlock?
    
    init(completion: GetSettingsCompletionBlock? = nil) {
        super.init()
        
        self.completion = completion
        
        completionBlock = { [weak self] in
            self?.completion?(self)
        }
    }

    override func main() {
        
        isExecuting = true
        getSwitchSettingFromRemote()
    }
    
    private func retryNetworkRequest() {

        if retries > 0 {
            print("Retrying GET request")
            getSwitchSettingFromRemote()
        }
        else {
            print("Aborting GET request, retries reached")
            isExecuting = false
            isFinished = true
        }
        
        retries -= 1
    }
    
    private func getSwitchSettingFromRemote() {
        
        print("Sending GET request to Server")
    
        let dataTask = URLSession.shared.dataTask(with: Network.baseUrl) { [weak self] (data: Data?, response: URLResponse?, error: Error?) in
            
            print("GET request respons from Server")
            
            if error != nil {
                self?.retryNetworkRequest()
            }
            else {
                
                guard let data = data else {
                    self?.retryNetworkRequest()
                    return
                }
                
                do {
                    let switchState = try JSONDecoder().decode(SwitchState.self, from: data)
                    
                    if switchState.state != nil { //Sanity check to make sure we get some correct data from service.
                        self?.completedState = switchState
                        self?.isExecuting = false
                        self?.isFinished = true
                    }
                    else {
                        self?.retryNetworkRequest()
                    }
                }
                catch _ {
                    self?.retryNetworkRequest()
                }
            }
        }
        
        dataTask.resume()
    }
}
