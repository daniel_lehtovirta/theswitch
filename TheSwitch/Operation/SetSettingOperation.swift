//
//  SetSettingOperation.swift
//  TheSwitch
//
//  Created by Daniel Lehtovirta on 23/04/2019.
//  Copyright © 2019 Daniel Lehtovirta. All rights reserved.
//

import UIKit

typealias SetSettingsCompletionBlock = (_ operation: SetSettingOperation?) -> Void

class SetSettingOperation: AsynchronousOperation {
    
    var urlRequest = URLRequest(url: Network.baseUrl)

    var newState: SwitchState!
    var completedState: SwitchState?
    
    var completion: SetSettingsCompletionBlock?
    
    init(switchState: Bool, completion: SetSettingsCompletionBlock? = nil) {
        super.init()
        
        newState = SwitchState(state: switchState)
        self.completion = completion
        
        completionBlock = { [weak self] in
            self?.completion?(self)
        }
    }
    
    override func main() {
        
        isExecuting = true
        
        urlRequest.httpMethod = HTTPMethod.put
        urlRequest.setValue(HeaderSetting.contentTypeJSON, forHTTPHeaderField: HeaderSetting.contentTypeHeader)

        do {
            urlRequest.httpBody =  try JSONEncoder().encode(newState)
            putSwitchSettingToRemote()
        }
        catch _ { //Failed to convert Bool to state model
            isExecuting = false
            isFinished = true
        }
    }
    
    private func retryNetworkRequest() {
        
        if retries > 0 {
            print("Retrying PUT request")
            putSwitchSettingToRemote()
        }
        else {
            print("Aborting PUT request, retries reached")
            isExecuting = false
            isFinished = true
        }
        
        retries -= 1
    }
    
    private func putSwitchSettingToRemote() {
        
        print("Sending PUT request to Server")
        
        let dataTask = URLSession.shared.dataTask(with: self.urlRequest) { [weak self] (data: Data?, response: URLResponse?, error: Error?) in
            
            print("PUT request respons from Server")
            
            if error != nil {
                self?.retryNetworkRequest()
            }
            else {
                
                guard let data = data else {
                    self?.retryNetworkRequest()
                    return
                }
                
                do {
                    let completedState = try JSONDecoder().decode(SwitchState.self, from: data)
                    
                    if completedState.state != nil { //Sanity check if we really got a state from service
                        self?.completedState = completedState
                        self?.isExecuting = false
                        self?.isFinished = true
                    }
                    else {
                        self?.retryNetworkRequest()
                    }

                }
                catch _ {
                    self?.retryNetworkRequest()
                }
            }
        }
        
        dataTask.resume()
    }
}
