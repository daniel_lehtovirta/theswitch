//
//  AsynchronousOperation.swift
//  TheSwitch
//
//  Created by Daniel Lehtovirta on 23/04/2019.
//  Copyright © 2019 Daniel Lehtovirta. All rights reserved.
//

import UIKit

class AsynchronousOperation: Operation {
    
    var _isFinished: Bool = false
    var _isExecuting: Bool = false
    
    var retries: Int = Network.networkRetries
    
    override var isFinished: Bool {
        
        set {
            willChangeValue(forKey: "isFinished")
            _isFinished = newValue
            didChangeValue(forKey: "isFinished")
        }
        
        get {
            return _isFinished
        }
    }
    
    override var isExecuting: Bool {
        set {
            willChangeValue(forKey: "isExecuting")
            _isExecuting = newValue
            didChangeValue(forKey: "isExecuting")
        }
        
        get {
            return _isExecuting
        }
    }
    
    override var isAsynchronous: Bool {
        return true
    }
}
