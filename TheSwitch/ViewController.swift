//
//  ViewController.swift
//  TheSwitch
//
//  Created by Daniel Lehtovirta on 23/04/2019.
//  Copyright © 2019 Daniel Lehtovirta. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let networkQueue = OperationQueue()
    
    let remoteLabel: UILabel = UILabel()
    let remoteSwitch: UISwitch = UISwitch()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        networkQueue.maxConcurrentOperationCount = 1
        
        remoteLabel.text = "The Switch"
        remoteLabel.font = UIFont.systemFont(ofSize: 20)
        remoteLabel.textColor = .black
        view.addSubview(remoteLabel)
        remoteLabel.translatesAutoresizingMaskIntoConstraints = false
        remoteLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        remoteLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true

        view.addSubview(remoteSwitch)
        remoteSwitch.isOn = LocalSettingStore.switchState() //Use local storage of switch
        remoteSwitch.translatesAutoresizingMaskIntoConstraints = false
        remoteSwitch.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        remoteSwitch.topAnchor.constraint(equalTo: remoteLabel.bottomAnchor, constant: 20).isActive = true
        remoteSwitch.addTarget(self, action: #selector(onRemoteSwitchChanged), for: .valueChanged)
        
        //Just to be on the safe side we are scheduling a repeating timer to synchronize the data
        Timer.scheduledTimer(withTimeInterval: Network.scheduledTimer, repeats: true) { [weak self] (timer: Timer) in
            self?.synchronizeRemoteStore()
        }

        synchronizeRemoteStore()
    }
    
    @objc func onRemoteSwitchChanged(_ sender:UISwitch) {
        
        //Store a local copy of the current state
        LocalSettingStore.switchChanged(switchState: sender.isOn)
        
        putSettingToRemote()
    }
    
    func putSettingToRemote() {
        let setOperation = SetSettingOperation(switchState: LocalSettingStore.switchState())
        networkQueue.addOperation(setOperation)
    }
    
    func synchronizeRemoteStore() {
        
        // Lets try to synchronize the local and remote store
        let getSettingsOperation = GetSettingOperation { [weak self] (operation) in
            
            if let operation = operation, let currentState = operation.completedState?.state {
                
                //We asume that the local storage is the 'master' in case there is a difference between the states
                //If we are a difference we send a update request
                if currentState != LocalSettingStore.switchState() {
                    
                    //Local storage is different from remote, lets synchronize
                    self?.putSettingToRemote()
                }
            }
            else { //Something else broke ('undefined' state, lets force current switch state to remote)
                self?.putSettingToRemote()
            }
        }
        
        networkQueue.addOperation(getSettingsOperation)
    }
}
